package de.fhwedel.oodb;

import de.fhwedel.oodb.ueb01.Ueb01;
import de.fhwedel.oodb.ueb02.Ueb02;
import de.fhwedel.oodb.ueb03.Ueb03;

public class Main {

    public static void main(final String[] args) {
        Ueb03.start();

    }

}
