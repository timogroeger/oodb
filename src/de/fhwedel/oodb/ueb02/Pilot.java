package de.fhwedel.oodb.ueb02;

public class Pilot {
    private final String m_name;
    private int          m_points;

    public Pilot(final String name, final int points) {
        this.m_name = name;
        this.m_points = points;
    }

    public void addPoints(final int points) {
        this.m_points += points;
    }

    public String getName() {
        return this.m_name;
    }

    public int getPoints() {
        return this.m_points;
    }

    @Override
    public String toString() {
        return this.m_name + ": " + this.m_points;
    }
}
