package de.fhwedel.oodb.ueb02;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Constraint;
import com.db4o.query.Predicate;
import com.db4o.query.Query;

import de.fhwedel.oodb.common.Commons;

public class Ueb02 {

	/* Name der Datenbankdatei. */
	private static String dbName = "ueb02.db";

	/* Statisch initialisierte Liste mit Piloten */
	private static final ArrayList<Pilot> pilotList = new ArrayList<>(Arrays.asList(new Pilot("Michael", 120),
			new Pilot("Rubens", 70), new Pilot("Jenson", 140), new Pilot("Nick", 1), new Pilot("John", 100)));

	private static final ArrayList<Integer> points = new ArrayList<>(Arrays.asList(1, 90, 100));

	private static final ArrayList<Object> sodaPoints = new ArrayList<>(Arrays.asList(1,90,100));

	/**
	 * Initialisiert die Datenbank
	 */
	private static void initDatabase() {
		new File(Ueb02.dbName).delete();
		final ObjectContainer db = Db4o.openFile(Ueb02.dbName);
		try {
			// Create
			System.out.println("Initialising database...");
			pilotList.forEach(db::store);
			System.out.println("Database content:");
			Commons.printObjects(getAllPilots(db));

		} finally {
			db.close();
		}
	}

	private static void doNativeQueries() {
		// Pr�dikate bauen
		Predicate<Pilot> p1 = new Predicate<Pilot>() {
			public boolean match(Pilot pilot) {
				return pilot.getPoints() == 100;
			}
		};

		Predicate<Pilot> p2 = new Predicate<Pilot>() {
			public boolean match(Pilot pilot) {
				return (((pilot.getPoints() >= 99) && (pilot.getPoints() <= 199)) || pilot.getName().equals("Rubens"));
			}
		};

		Predicate<Pilot> p3 = new Predicate<Pilot>() {
			public boolean match(Pilot pilot) {
				return (points.contains(pilot.getPoints()));
			}
		};

		final ObjectContainer db = Db4o.openFile(Ueb02.dbName);
		try {
			System.out.println("Punktzahl = 100");
			Commons.printObjects(db.query(p1));

			System.out.println("Punktzahl 99 - 199 ODER Fahrername == 'Rubens'");
			Commons.printObjects(db.query(p2));

			System.out.println("Punktzahl in Liste enthalten [1,90,100]");
			Commons.printObjects(db.query(p3));
		} finally {
			db.close();
		}
	}

	// SODA-Abfrage
	private static void doSodaQueries() {
		final ObjectContainer db = Db4o.openFile(Ueb02.dbName);

		// Queries aufbauen
		Query q1 = makeQueryConstraint(db, "m_points", new Integer(100));
		Query q2 = makeQueryConstraint(db, Pilot.class);
		Query q3 = makeQueryConstraint(db, Pilot.class);

		Constraint c2MaxPoints = q2.descend("m_points").constrain(new Integer(200)).smaller();
		Constraint c2name = q2.descend("m_name").constrain("Rubens");

		q2.descend("m_points").constrain(new Integer(98)).greater().and(c2MaxPoints).or(c2name);

		try {
			System.out.println("SODA: Punkte = 100");
			Commons.printObjects(q1.execute());
			System.out.println("SODA: Punktzahl 99 - 199 ODER Fahrername == 'Rubens'");
			Commons.printObjects(q2.execute());
			System.out.println("SODA: Punktzahl in Liste enthalten [1,90,100] ");
			Commons.printObjects(buildORConstraint(q3, "m_points", sodaPoints).execute());
		} finally {
			db.close();
		}
	}

	@SuppressWarnings("deprecation")
	public static void start() {
		initDatabase();

		doNativeQueries();

		doSodaQueries();

		whipeDatabase();
	}

	/**
	 * L�dt alle Piloten aus der Datenbank mithilfe eines Beispielobjektes einer
	 * Klasse.
	 * 
	 * @param db
	 * @return
	 */
	private static ObjectSet<Pilot> getAllPilots(ObjectContainer db) {
		return db.queryByExample(Pilot.class);
	}

	/**
	 * L�scht alle Objekte in der Datenbank.
	 */
	private static void whipeDatabase() {
		final ObjectContainer db = Db4o.openFile(Ueb02.dbName);
		try {
			System.out.println("Deleting all Pilots...");
			db.queryByExample(null).forEach(db::delete);
			Commons.printDB(db);
		} finally {
			db.close();
		}
	}

	private static Query makeQueryConstraint(ObjectContainer db, Class c) {
		Query q = db.query();
		q.constrain(c);
		return q;
	}

	private static Query makeQueryConstraint(ObjectContainer db, String attribute, Object value) {
		Query q = db.query();
		q.descend(attribute).constrain(value);
		return q;
	}

	private static Query buildORConstraint(Query q, String attribute, List<Object> valueList) {
		
		Constraint res = q.descend(attribute).constrain(valueList.get(0));

		for (int i = 1; i < valueList.size(); i++)
			res = q.descend(attribute).constrain(valueList.get(i)).or(res);

		return q;
	}

}
