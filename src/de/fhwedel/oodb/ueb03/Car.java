package de.fhwedel.oodb.ueb03;


public class Car {

    private String model;
    private Pilot pilot;

    public Car(String model, Pilot pilot) {
        this.model = model;
        this.pilot = pilot;
    }

    public Car(String model) {
        this(model, null);
    }

    public String getModel() {
        return model;
    }

    public Pilot getPilot() {
        return pilot;
    }
    
    public void setModel(String newModel) {
    	this.model = newModel;
    }

	@Override
	public String toString() {
		return "Car [model=" + model + ", pilot=" + pilot + "]";
	}
    
    

}
