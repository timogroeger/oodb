package de.fhwedel.oodb.ueb03;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Constraint;
import com.db4o.query.Predicate;
import com.db4o.query.Query;

import de.fhwedel.oodb.common.Commons;

public class Ueb03 {

	/* Name der Datenbankdatei. */
	private static String dbName = "ueb03.db";

	/* Statisch initialisierte Liste mit Piloten */
	private static final ArrayList<Pilot> pilotList = new ArrayList<>(Arrays.asList(new Pilot("Michael", 120),
			new Pilot("Rubens", 70), new Pilot("Jenson", 140), new Pilot("Nick", 1), new Pilot("John", 100)));

	/* Statisch initialisierte Liste mit Piloten */
	private static final ArrayList<Car> carList = new ArrayList<>(Arrays.asList(new Car("Mercedes", pilotList.get(0)),
			new Car("BMW", pilotList.get(1)), new Car("Ferrari", pilotList.get(2)), new Car("Opel", pilotList.get(3)),
			new Car("Lamborghini", pilotList.get(1))));

	private static final ArrayList<Integer> points = new ArrayList<>(Arrays.asList(1, 90, 100));

	/**
	 * Initialisiert die Datenbank
	 */
	private static void initDatabase(ObjectContainer db) {
		new File(Ueb03.dbName).delete();

		// Create
		System.out.println("Initialising database...");
		carList.forEach(db::store);

	}

	@SuppressWarnings("deprecation")
	public static void start() {
		ObjectContainer db = Commons.makeDatabaseConnection(dbName, Car.class);
		try {

			initDatabase(db);

			// Autos laden und ausgeben
			ObjectSet<Car> cars = Commons.getObjectsByClass(db, Car.class);
			Commons.printDB(db);

			// Einen Piloten �ndern
			cars.get(0).getPilot().addPoints(5000);
			// Zur Kontrolle: Ein Auto �ndern
			cars.get(0).setModel("FIAT PUNTO");
			System.out.println("Gib Michael 5000 Punkte, �ndere sein Auto zum Fiat Punto.");

			db.store(cars.get(0));

		} finally {
			db.close();
		}

		System.out.println("Datenbank geschlossen. Wird wieder ge�ffnet...");

		// Datenbank schlie�en und �ffnen, Inhalte anschauen.
		db = Commons.makeDatabaseConnection(dbName, Car.class);
		try {
			ObjectSet<Car> cars = Commons.getObjectsByClass(db, Car.class);
			Commons.printDB(db);
			
			System.out.println("L�sche den BMW. Erwarte, dass Rubens auch gel�scht wird.");
			db.delete(cars.get(1));

		} finally {
			db.close();
		}

		System.out.println("Datenbank geschlossen. Wird wieder ge�ffnet...");

		// Datenbank schlie�en und �ffnen, Inhalte anschauen.
		db = Commons.makeDatabaseConnection(dbName);
		try {
			Commons.printDB(db);
		} finally {
			db.close();
		}

		Commons.whipeDatabase(dbName);

	}

}
