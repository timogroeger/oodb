package de.fhwedel.oodb.common;

import com.db4o.Db4o;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.Configuration;
import com.db4o.config.ConfigurationItem;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.internal.InternalObjectContainer;

import de.fhwedel.oodb.ueb02.Ueb02;
import de.fhwedel.oodb.ueb03.Ueb03;

public class Commons {

	/**
	 * Gibt den Inhalt einer Datenbank aus.
	 * 
	 * @param db
	 */
	public static void printDB(final ObjectContainer db) {
		System.out.println("Database content:");
		printObjects(db.queryByExample(null));
		System.out.println("");
	}

	/**
	 * Gibt alle Objekte einer Objektmenge aus.
	 * 
	 * @param o
	 */
	public static void printObjects(final ObjectSet<?> o) {
		for (final Object objs : o) {
			System.out.println(objs.toString());
		}
		System.out.println("");
	}

	/**
	 * Liefert ein ObjectSet mit allen Objekten der angegebenen Klasse.
	 * 
	 * @param <T>
	 * @param db
	 * @param c
	 * @return
	 */
	public static ObjectSet getObjectsByClass(ObjectContainer db, final Class c) {
		ObjectSet res = db.queryByExample(c);
		return res;
	}

	/**
	 * L�scht alle Objekte in der Datenbank.
	 */
	public static void whipeDatabase(String dbName) {
		final ObjectContainer db = Db4o.openFile(dbName);
		try {
			System.out.println("Cleaning database...");
			db.queryByExample(null).forEach(db::delete);
			Commons.printDB(db);
		} finally {
			db.close();
		}
	}

	@SuppressWarnings("deprecation")
	public static ObjectContainer makeDatabaseConnection(String db, Class... classes) {
		EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
		for (Class c : classes) {
			config.common().objectClass(c).cascadeOnUpdate(true);
			config.common().objectClass(c).cascadeOnDelete(true);
		}

		return Db4oEmbedded.openFile(config, db);
	}
}
