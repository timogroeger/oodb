package de.fhwedel.oodb.ueb01;

import java.io.File;
import java.util.ArrayList;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Ueb01 {

    private static String dbName = "ueb01.db";

    private static void printDB(final ObjectContainer db) {
        System.out.println("Database content:");
        Ueb01.printObjects(db.queryByExample(null));
        System.out.println("--------------");
    }

    public static void printObjects(final ObjectSet<Object> o) {
        for (final Object objs : o) {
            System.out.println(objs.toString());
        }
    }

    @SuppressWarnings("deprecation")
    public static void start() {
        new File(Ueb01.dbName).delete();
        final ObjectContainer db = Db4o.openFile(Ueb01.dbName);
        final ArrayList<Pilot> pilotList = new ArrayList<>();

        pilotList.add(new Pilot("Michael", 0));
        pilotList.add(new Pilot("Rubens", 0));
        pilotList.add(new Pilot("Jenson", 0));
        pilotList.add(new Pilot("Nick", 0));

        try {
            // Create
            System.out.println("Adding Objects to DB...");
            pilotList.forEach(db::store);

            Ueb01.printDB(db);

            // Read
            System.out.println("Reading all Objects from DB...");
            final ObjectSet<Object> o = db.queryByExample(new Pilot(null, 0));
            System.out.println("Result:");
            Ueb01.printObjects(o);
            System.out.println("--------------");

            // Update
            System.out.println("Updating Michael...");
            final ObjectSet<Object> o2 = db.queryByExample(pilotList.get(0));
            final Pilot p = (Pilot) o2.get(0);
            p.addPoints(10);
            db.store(p);

            Ueb01.printDB(db);

            // Delete
            System.out.println("Deleting all Pilots...");
            pilotList.forEach(db::delete);

            Ueb01.printDB(db);

        } finally {
            db.close();
        }
    }
}
